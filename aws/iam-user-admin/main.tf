terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.49.0"
    }
  }
}

provider "aws" {
  region = var.region
}

variable "region" {
  type    = string
  default = "us-east-1"
}

variable "username" {
  type        = string
  description = "username of user you're creating."
}

resource "aws_iam_user" "iam-admin" {
  name = var.username
  path = "/"
}

variable "usergroup" {
  type        = list(string)
  description = "list of groups to add the user to"
}

resource "aws_iam_user_group_membership" "group" {
  user   = aws_iam_user.iam-admin.name
  groups = var.usergroup
}

resource "aws_iam_access_key" "iam-admin" {
  user = aws_iam_user.iam-admin.name
}

output "key" {
  value = aws_iam_access_key.iam-admin.id
}

output "secret" {
  value     = aws_iam_access_key.iam-admin.secret
  sensitive = true
}

output "user" {
  value = aws_iam_access_key.iam-admin.user
}
