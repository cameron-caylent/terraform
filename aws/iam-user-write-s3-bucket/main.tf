terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.49.0"
    }
  }
}

variable "region" {
  type    = string
  default = "us-east-1"
}

provider "aws" {
  # Configuration options
  region = var.region
}

resource "aws_iam_user" "s3-wo" {
  name = "s3-wo"
  path = "/"
}

variable "bucket-list" {
  type = list(string)
}

resource "aws_iam_access_key" "s3-wo" {
  user = aws_iam_user.s3-wo.name
}

data "aws_iam_policy_document" "s3-wo" {
  statement {
    sid    = "PutObject"
    effect = "Allow"
    actions = [
      "s3:*Object"
    ]
    resources = var.bucket-list
  }
}

resource "aws_iam_policy" "s3-wo" {
  name   = "s3-wo"
  path   = "/"
  policy = data.aws_iam_policy_document.s3-wo.json
}

resource "aws_iam_user_policy" "s3-wo" {
  name   = "s3-wo"
  user   = aws_iam_user.s3-wo.name
  policy = aws_iam_policy.s3-wo.policy
}

output "secret" {
  value     = aws_iam_access_key.s3-wo.secret
  sensitive = true
}

output "key" {
  value = aws_iam_access_key.s3-wo.id
}

output "user" {
  value = aws_iam_access_key.s3-wo.user
}

output "create-date" {
  value = aws_iam_access_key.s3-wo.create_date
}
